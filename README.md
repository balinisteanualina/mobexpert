```{=html}
<?xml version="1.0" encoding="UTF-8"?>
```
`<run>`{=html} `<id>`{=html}R32`</id>`{=html} `<name>`{=html}Ca
utilizator vreau sa imi pot crea cont pe site pentru a putea plasa
comenzi online`</name>`{=html}
`<description>`{=html}`</description>`{=html}
`<config>`{=html}`</config>`{=html}
`<createdon>`{=html}2022-06-23T21:08:11Z`</createdon>`{=html}
`<completed>`{=html}false`</completed>`{=html}
`<milestone>`{=html}`</milestone>`{=html} `<stats>`{=html}
`<passed>`{=html} `<percent>`{=html}60`</percent>`{=html}
`<count>`{=html}3`</count>`{=html} `</passed>`{=html} `<blocked>`{=html}
`<percent>`{=html}0`</percent>`{=html}
`<count>`{=html}0`</count>`{=html} `</blocked>`{=html}
`<untested>`{=html} `<percent>`{=html}20`</percent>`{=html}
`<count>`{=html}1`</count>`{=html} `</untested>`{=html}
`<retest>`{=html} `<percent>`{=html}0`</percent>`{=html}
`<count>`{=html}0`</count>`{=html} `</retest>`{=html} `<failed>`{=html}
`<percent>`{=html}20`</percent>`{=html}
`<count>`{=html}1`</count>`{=html} `</failed>`{=html} `</stats>`{=html}
`<sections>`{=html}
```{=html}
<section>
```
`<name>`{=html}Test Cases`</name>`{=html}
`<description>`{=html}`</description>`{=html} `<tests>`{=html}
`<test>`{=html} `<id>`{=html}T294`</id>`{=html}
```{=html}
<title>
```
Verificarea existentei si functionalitatii butonului \"Contul meu\"
```{=html}
</title>
```
                                        <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-1</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                        <preconds>1.Sa avem conexiune la internet</preconds>
                                        <steps_separated><step>

`<index>`{=html}1`</index>`{=html}`<content>`{=html}`</content>`{=html}`<expected>`{=html}`</expected>`{=html}`</step>`{=html}
`</steps_separated>`{=html} `</custom>`{=html}
`<caseid>`{=html}C9`</caseid>`{=html}
`<status>`{=html}Passed`</status>`{=html} `<assignedto>`{=html}Tester
Ro29`</assignedto>`{=html} `<inprogress>`{=html}`</inprogress>`{=html}
`<changes>`{=html} `<change>`{=html}
`<createdon>`{=html}2022-06-24T19:44:19Z`</createdon>`{=html}
`<createdby>`{=html}Tester Ro29`</createdby>`{=html}
`<status>`{=html}Passed`</status>`{=html}
`<assignedto>`{=html}`</assignedto>`{=html}
`<comment>`{=html}`</comment>`{=html}
`<version>`{=html}`</version>`{=html}
`<elapsed>`{=html}`</elapsed>`{=html}
`<defects>`{=html}`</defects>`{=html} `<custom>`{=html}
`<step_results>`{=html}`<step>`{=html}
`<content>`{=html}`</content>`{=html}`<expected>`{=html}`</expected>`{=html}`<actual>`{=html}`</actual>`{=html}`<status>`{=html}Untested`</status>`{=html}`</step>`{=html}
`</step_results>`{=html} `</custom>`{=html} `</change>`{=html}
`<change>`{=html}
`<createdon>`{=html}2022-06-23T21:08:11Z`</createdon>`{=html}
`<createdby>`{=html}Tester Ro29`</createdby>`{=html}
`<status>`{=html}Untested`</status>`{=html}
`<assignedto>`{=html}`</assignedto>`{=html}
`<comment>`{=html}`</comment>`{=html}
`<version>`{=html}`</version>`{=html}
`<elapsed>`{=html}`</elapsed>`{=html}
`<defects>`{=html}`</defects>`{=html} `</change>`{=html}
`</changes>`{=html} `</test>`{=html} `<test>`{=html}
`<id>`{=html}T295`</id>`{=html}
```{=html}
<title>
```
Verificarea functionalitatii butonului \"Login cu Facebook\" si logarea
in cont
```{=html}
</title>
```
                                        <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-1</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C31</caseid>
                                                                <status>Untested</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-23T21:08:11Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                                                                                                                                                                                                                                <test>
                <id>T296</id>
                <title>Verificarea functionalitatii butonului &quot;Login cu Google&quot; si logarea in cont</title>
                                                <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-1</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                        <preconds>1.Existenta unei adrese de google de email valida si sa nu fie logata in cont.

`</preconds>`{=html} `<steps_separated>`{=html}`<step>`{=html}
`<index>`{=html}1`</index>`{=html}`<content>`{=html}Accesare URL
https://mobexpert.ro/`</content>`{=html}`<expected>`{=html}Se va
deschide home page-ul site-ului `</expected>`{=html}`</step>`{=html}
`<step>`{=html}
`<index>`{=html}2`</index>`{=html}`<content>`{=html}Accesare buton
\"Contul meu\"`</content>`{=html}`<expected>`{=html}Se deschide o pagina
noua cu modalitatile de logare`</expected>`{=html}`</step>`{=html}
`<step>`{=html}
`<index>`{=html}3`</index>`{=html}`<content>`{=html}Accesare buton
\"Login cu Google\"`</content>`{=html}`<expected>`{=html}Se deschide o
noua pagina in care va fi trebui sa fie introdusa adresa de email
`</expected>`{=html}`</step>`{=html} `<step>`{=html}
`<index>`{=html}4`</index>`{=html}`<content>`{=html}Introducere adresa
de email corecta`</content>`{=html}`<expected>`{=html}Se deschide o noua
pagina in care va trebui sa fie introdusa
parola`</expected>`{=html}`</step>`{=html} `<step>`{=html}
`<index>`{=html}5`</index>`{=html}`<content>`{=html}Introducere parola
corecta `</content>`{=html}`<expected>`{=html}Logare cu
succes`</expected>`{=html}`</step>`{=html} `</steps_separated>`{=html}
`</custom>`{=html} `<caseid>`{=html}C32`</caseid>`{=html}
`<status>`{=html}Passed`</status>`{=html} `<assignedto>`{=html}Tester
Ro29`</assignedto>`{=html} `<inprogress>`{=html}`</inprogress>`{=html}
`<changes>`{=html} `<change>`{=html}
`<createdon>`{=html}2022-06-24T19:50:01Z`</createdon>`{=html}
`<createdby>`{=html}Tester Ro29`</createdby>`{=html}
`<status>`{=html}Passed`</status>`{=html}
`<assignedto>`{=html}`</assignedto>`{=html}
`<comment>`{=html}`</comment>`{=html}
`<version>`{=html}`</version>`{=html}
`<elapsed>`{=html}`</elapsed>`{=html}
`<defects>`{=html}`</defects>`{=html} `<custom>`{=html}
`<step_results>`{=html}`<step>`{=html} `<content>`{=html}Accesare URL
https://mobexpert.ro/`</content>`{=html}`<expected>`{=html}Se va
deschide home page-ul site-ului `</expected>`{=html}`<actual>`{=html}S-a
deschis pagina principala a
site-ului`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Accesare buton \"Contul
meu\"`</content>`{=html}`<expected>`{=html}Se deschide o pagina noua cu
modalitatile de logare`</expected>`{=html}`<actual>`{=html}S-a deschis o
noua pagina cu modalitatile de
logare`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Accesare buton \"Login cu
Google\"`</content>`{=html}`<expected>`{=html}Se deschide o noua pagina
in care va fi trebui sa fie introdusa adresa de email
`</expected>`{=html}`<actual>`{=html}S-a deschis o noua pagina in care
se poate introduce adresa de email
valida`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Introducere adresa de email
corecta`</content>`{=html}`<expected>`{=html}Se deschide o noua pagina
in care va trebui sa fie introdusa
parola`</expected>`{=html}`<actual>`{=html}S-a deschis o noua pagina in
care se poate introduce parola
corecta`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Introducere parola corecta
`</content>`{=html}`<expected>`{=html}Logare cu
succes`</expected>`{=html}`<actual>`{=html}Utilizatorul este logat in
cont`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`</step_results>`{=html} `</custom>`{=html} `</change>`{=html}
`<change>`{=html}
`<createdon>`{=html}2022-06-23T21:08:11Z`</createdon>`{=html}
`<createdby>`{=html}Tester Ro29`</createdby>`{=html}
`<status>`{=html}Untested`</status>`{=html}
`<assignedto>`{=html}`</assignedto>`{=html}
`<comment>`{=html}`</comment>`{=html}
`<version>`{=html}`</version>`{=html}
`<elapsed>`{=html}`</elapsed>`{=html}
`<defects>`{=html}`</defects>`{=html} `</change>`{=html}
`</changes>`{=html} `</test>`{=html} `<test>`{=html}
`<id>`{=html}T297`</id>`{=html}
```{=html}
<title>
```
Verifica campurile: \"Prenume\", \"Nume\", \"Email\", \"Parola\"
```{=html}
</title>
```
                                        <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-1</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C33</caseid>
                                                                <status>Passed</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-24T19:50:10Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Passed</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                                            <change>
                        <createdon>2022-06-23T21:08:11Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                                                                                                                                                                                                                                <test>
                <id>T298</id>
                <title>Verificarea functionalitatii butonului &quot;Fa-ti cont&quot; si crearea contului</title>
                                                <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-13</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                        <steps_separated><step>

`<index>`{=html}1`</index>`{=html}`<content>`{=html}Accesare Url:
https://mobexpert.ro/`</content>`{=html}`<expected>`{=html}Se deschide
pagina principala a site-ului`</expected>`{=html}`</step>`{=html}
`<step>`{=html}
`<index>`{=html}2`</index>`{=html}`<content>`{=html}Accesare buton
\"Contul meu\"`</content>`{=html}`<expected>`{=html}Se deschide o pagina
cu modalitati de logare sau creare
cont`</expected>`{=html}`</step>`{=html} `<step>`{=html}
`<index>`{=html}3`</index>`{=html}`<content>`{=html}Se completeaza
campul \"Prenume\": Alina`</content>`{=html}`<expected>`{=html}Se
afiseaza prenumele completat: Alina, campul este
editabil`</expected>`{=html}`</step>`{=html} `<step>`{=html}
`<index>`{=html}4`</index>`{=html}`<content>`{=html}Se completeaza
campul &ldquo;Nume&rdquo;:
Balinisteanu`</content>`{=html}`<expected>`{=html}Se afiseaza numele
completat: Balinisteanu, campul este
editabil`</expected>`{=html}`</step>`{=html} `<step>`{=html}
`<index>`{=html}5`</index>`{=html}`<content>`{=html}Se completeaza
campul &ldquo;Email*&rdquo; cu credentiale:
&ldquo;balinisteanualina\@gmail.com&rdquo;`</content>`{=html}`<expected>`{=html}Se
afiseaza adresa de email &ldquo;balinisteanualina\@gmail.com &rdquo;,
campul este editabil`</expected>`{=html}`</step>`{=html} `<step>`{=html}
`<index>`{=html}6`</index>`{=html}`<content>`{=html}Se completeaza
campul &ldquo;Parola*&rdquo; cu credentiale:
&ldquo;\*\*\*\*\*\*\*\*\*\*&rdquo;`</content>`{=html}`<expected>`{=html}Se
afiseaza parola`</expected>`{=html}`</step>`{=html} `<step>`{=html}
`<index>`{=html}7`</index>`{=html}`<content>`{=html}Se face click pe
butonul: \"Fa-ti cont!\"`</content>`{=html}`<expected>`{=html}Se creeaza
contul`</expected>`{=html}`</step>`{=html} `</steps_separated>`{=html}
`</custom>`{=html} `<caseid>`{=html}C34`</caseid>`{=html}
`<status>`{=html}Failed`</status>`{=html} `<assignedto>`{=html}Tester
Ro29`</assignedto>`{=html} `<inprogress>`{=html}`</inprogress>`{=html}
`<defects>`{=html}AB-13`</defects>`{=html} `<changes>`{=html}
`<change>`{=html}
`<createdon>`{=html}2022-06-24T19:56:33Z`</createdon>`{=html}
`<createdby>`{=html}Tester Ro29`</createdby>`{=html}
`<status>`{=html}Failed`</status>`{=html}
`<assignedto>`{=html}`</assignedto>`{=html}
`<comment>`{=html}`</comment>`{=html}
`<version>`{=html}`</version>`{=html}
`<elapsed>`{=html}`</elapsed>`{=html}
`<defects>`{=html}AB-13`</defects>`{=html} `<custom>`{=html}
`<step_results>`{=html}`<step>`{=html} `<content>`{=html}Accesare Url:
https://mobexpert.ro/`</content>`{=html}`<expected>`{=html}Se deschide
pagina principala a site-ului`</expected>`{=html}`<actual>`{=html}S-a
deschis pagina principala a
site-ului`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Accesare buton \"Contul
meu\"`</content>`{=html}`<expected>`{=html}Se deschide o pagina cu
modalitati de logare sau creare
cont`</expected>`{=html}`<actual>`{=html}S-a deschis o noua pagina cu
modalitatile de
logare`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Se completeaza campul \"Prenume\":
Alina`</content>`{=html}`<expected>`{=html}Se afiseaza prenumele
completat: Alina, campul este
editabil`</expected>`{=html}`<actual>`{=html}Campul \"prenume\"este
editabil apare prenumele
introdus`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Se completeaza campul
&ldquo;Nume&rdquo;: Balinisteanu`</content>`{=html}`<expected>`{=html}Se
afiseaza numele completat: Balinisteanu, campul este
editabil`</expected>`{=html}`<actual>`{=html}Campul \"Nume\" este
editabil apare numele
introdus`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Se completeaza campul
&ldquo;Email*&rdquo; cu credentiale:
&ldquo;balinisteanualina\@gmail.com&rdquo;`</content>`{=html}`<expected>`{=html}Se
afiseaza adresa de email &ldquo;balinisteanualina\@gmail.com &rdquo;,
campul este editabil`</expected>`{=html}`<actual>`{=html}Campul
\"Email\" este editabil apare emailul
introdus`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Se completeaza campul
&ldquo;Parola*&rdquo; cu credentiale:
&ldquo;\*\*\*\*\*\*\*\*\*\*&rdquo;`</content>`{=html}`<expected>`{=html}Se
afiseaza parola`</expected>`{=html}`<actual>`{=html}Campul \"Parola\"
este editabil parola este
introdusa`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Se face click pe butonul: \"Fa-ti
cont!\"`</content>`{=html}`<expected>`{=html}Se creeaza
contul`</expected>`{=html}`<actual>`{=html}Contul nu se creeaza si se
reseteaza pagina anuland astfel datele introduse in campurile mentionate
mai
sus.`</actual>`{=html}`<status>`{=html}Failed`</status>`{=html}`</step>`{=html}
`</step_results>`{=html} `</custom>`{=html} `</change>`{=html}
`<change>`{=html}
`<createdon>`{=html}2022-06-23T21:08:11Z`</createdon>`{=html}
`<createdby>`{=html}Tester Ro29`</createdby>`{=html}
`<status>`{=html}Untested`</status>`{=html}
`<assignedto>`{=html}`</assignedto>`{=html}
`<comment>`{=html}`</comment>`{=html}
`<version>`{=html}`</version>`{=html}
`<elapsed>`{=html}`</elapsed>`{=html}
`<defects>`{=html}`</defects>`{=html} `</change>`{=html}
`</changes>`{=html} `</test>`{=html} `</tests>`{=html}
```{=html}
</section>
```
`</sections>`{=html} `</run>`{=html}


```{=html}
<?xml version="1.0" encoding="UTF-8"?>
```
`<run>`{=html} `<id>`{=html}R33`</id>`{=html} `<name>`{=html}Ca
utilizator vreau sa imi pot crea o lista de produse
favorite`</name>`{=html} `<description>`{=html}`</description>`{=html}
`<config>`{=html}`</config>`{=html}
`<createdon>`{=html}2022-06-23T21:08:59Z`</createdon>`{=html}
`<completed>`{=html}false`</completed>`{=html}
`<milestone>`{=html}`</milestone>`{=html} `<stats>`{=html}
`<passed>`{=html} `<percent>`{=html}88`</percent>`{=html}
`<count>`{=html}7`</count>`{=html} `</passed>`{=html} `<blocked>`{=html}
`<percent>`{=html}0`</percent>`{=html}
`<count>`{=html}0`</count>`{=html} `</blocked>`{=html}
`<untested>`{=html} `<percent>`{=html}0`</percent>`{=html}
`<count>`{=html}0`</count>`{=html} `</untested>`{=html}
`<retest>`{=html} `<percent>`{=html}0`</percent>`{=html}
`<count>`{=html}0`</count>`{=html} `</retest>`{=html} `<failed>`{=html}
`<percent>`{=html}13`</percent>`{=html}
`<count>`{=html}1`</count>`{=html} `</failed>`{=html} `</stats>`{=html}
`<sections>`{=html}
```{=html}
<section>
```
`<name>`{=html}Test Cases`</name>`{=html}
`<description>`{=html}`</description>`{=html} `<tests>`{=html}
`<test>`{=html} `<id>`{=html}T301`</id>`{=html}
```{=html}
<title>
```
Vizualizarea produsului in pagina
```{=html}
</title>
```
                                        <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-3</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C70</caseid>
                                                                <status>Passed</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-24T19:35:48Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Passed</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                                            <change>
                        <createdon>2022-06-23T21:08:59Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                                                                                                                                                                                                                                <test>
                <id>T302</id>
                <title>Verificarea posibilitatii de selectare a culorii dorite</title>
                                                <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-3</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C71</caseid>
                                                                <status>Passed</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-24T19:35:58Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Passed</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                                            <change>
                        <createdon>2022-06-23T21:08:59Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                                                                                                                                                                                                                                <test>
                <id>T303</id>
                <title>Verificarea functionalitatii butonului “Adauga la favorite”</title>
                                                <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-3</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C78</caseid>
                                                                <status>Passed</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-24T19:36:34Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Passed</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                                            <change>
                        <createdon>2022-06-23T21:08:59Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                                                                                                                                                                                                                                <test>
                <id>T304</id>
                <title>Verficarea modificarii statusului simbolului inimioara</title>
                                                <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-12</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                        <steps_separated><step>

`<index>`{=html}1`</index>`{=html}`<content>`{=html}Accesare Url
https://mobexpert.ro/`</content>`{=html}`<expected>`{=html}Se deschide
pagina principla a site-ului`</expected>`{=html}`</step>`{=html}
`<step>`{=html} `<index>`{=html}2`</index>`{=html}`<content>`{=html}Se
face hover pe categoria \"Mobilier copii\" din pagina
principala`</content>`{=html}`<expected>`{=html}\
Apare un pop up cu diverse categorii de mobilier pentru
copii`</expected>`{=html}`</step>`{=html} `<step>`{=html}
`<index>`{=html}3`</index>`{=html}`<content>`{=html}Se apasa click pe
categoria \"Comode\"`</content>`{=html}`<expected>`{=html}Se deschide o
pagina noua cu toate produsele din categoria
selectata`</expected>`{=html}`</step>`{=html} `<step>`{=html}
`<index>`{=html}4`</index>`{=html}`<content>`{=html}Se apasa click pe
produsul \"NORDIC cufar jucarii
copii\"`</content>`{=html}`<expected>`{=html}Se deschide o noua pagina
doar cu produsul selectat`</expected>`{=html}`</step>`{=html}
`<step>`{=html} `<index>`{=html}5`</index>`{=html}`<content>`{=html}Se
face click pe simbolul \"Inimioara-Adauga la
favorite&rdquo;`</content>`{=html}`<expected>`{=html}Se va incrementa
simbolul inimioara cu un produs si se va colora
butonul`</expected>`{=html}`</step>`{=html} `</steps_separated>`{=html}
`</custom>`{=html} `<caseid>`{=html}C80`</caseid>`{=html}
`<status>`{=html}Failed`</status>`{=html} `<assignedto>`{=html}Tester
Ro29`</assignedto>`{=html} `<inprogress>`{=html}`</inprogress>`{=html}
`<defects>`{=html}AB-12`</defects>`{=html} `<changes>`{=html}
`<change>`{=html}
`<createdon>`{=html}2022-06-24T19:37:50Z`</createdon>`{=html}
`<createdby>`{=html}Tester Ro29`</createdby>`{=html}
`<status>`{=html}Failed`</status>`{=html}
`<assignedto>`{=html}`</assignedto>`{=html}
`<comment>`{=html}`</comment>`{=html}
`<version>`{=html}`</version>`{=html}
`<elapsed>`{=html}`</elapsed>`{=html}
`<defects>`{=html}AB-12`</defects>`{=html} `<custom>`{=html}
`<step_results>`{=html}`<step>`{=html} `<content>`{=html}Accesare Url
https://mobexpert.ro/`</content>`{=html}`<expected>`{=html}Se deschide
pagina principla a
site-ului`</expected>`{=html}`<actual>`{=html}`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Se face hover pe categoria \"Mobilier
copii\" din pagina principala`</content>`{=html}`<expected>`{=html}\
Apare un pop up cu diverse categorii de mobilier pentru
copii`</expected>`{=html}`<actual>`{=html}`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Se apasa click pe categoria
\"Comode\"`</content>`{=html}`<expected>`{=html}Se deschide o pagina
noua cu toate produsele din categoria
selectata`</expected>`{=html}`<actual>`{=html}`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Se apasa click pe produsul \"NORDIC
cufar jucarii copii\"`</content>`{=html}`<expected>`{=html}Se deschide o
noua pagina doar cu produsul
selectat`</expected>`{=html}`<actual>`{=html}`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Se face click pe simbolul
\"Inimioara-Adauga la
favorite&rdquo;`</content>`{=html}`<expected>`{=html}Se va incrementa
simbolul inimioara cu un produs si se va colora
butonul`</expected>`{=html}`<actual>`{=html}Butonul \"Inimioara-Adauga
la favorite&amp;rdquo; nu se
coloreaza.`</actual>`{=html}`<status>`{=html}Failed`</status>`{=html}`</step>`{=html}
`</step_results>`{=html} `</custom>`{=html} `</change>`{=html}
`<change>`{=html}
`<createdon>`{=html}2022-06-23T21:08:59Z`</createdon>`{=html}
`<createdby>`{=html}Tester Ro29`</createdby>`{=html}
`<status>`{=html}Untested`</status>`{=html}
`<assignedto>`{=html}`</assignedto>`{=html}
`<comment>`{=html}`</comment>`{=html}
`<version>`{=html}`</version>`{=html}
`<elapsed>`{=html}`</elapsed>`{=html}
`<defects>`{=html}`</defects>`{=html} `</change>`{=html}
`</changes>`{=html} `</test>`{=html} `<test>`{=html}
`<id>`{=html}T305`</id>`{=html}
```{=html}
<title>
```
Se va verifica butonul inimioara din dreapta sus daca se incrementeaza
lista de produse favorite
```{=html}
</title>
```
                                        <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-3</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C83</caseid>
                                                                <status>Passed</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-24T19:38:05Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Passed</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                                            <change>
                        <createdon>2022-06-23T21:08:59Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                                                                                                                                                                                                                                <test>
                <id>T306</id>
                <title>Verificarea functionalitatii  “Butonului inimioara” din dreapa sus pentru a vizualiza lista de produse favorite</title>
                                                <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-3</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                        <steps_separated><step>

`<index>`{=html}1`</index>`{=html}`<content>`{=html}Accesare URL
https://mobexpert.ro/`</content>`{=html}`<expected>`{=html}Se va
deschide pagina principala a
site-ului`</expected>`{=html}`</step>`{=html} `<step>`{=html}
`<index>`{=html}2`</index>`{=html}`<content>`{=html}Selectare dintr-o
categorie de produse un anumit
produs`</content>`{=html}`<expected>`{=html}Se va deschide o noua pagina
in care va fi afisat produsul
selectat`</expected>`{=html}`</step>`{=html} `<step>`{=html}
`<index>`{=html}3`</index>`{=html}`<content>`{=html}Apasare buton
\"Adauga la favorite\"`</content>`{=html}`<expected>`{=html}Se va adauga
produsul in lista de favorite `</expected>`{=html}`</step>`{=html}
`<step>`{=html}
`<index>`{=html}4`</index>`{=html}`<content>`{=html}Afisare pagina cu
produs`</content>`{=html}`<expected>`{=html}In dreapta sus in butonul cu
lista de produse favorite se va modifica cantitatea de produse
favorite.`</expected>`{=html}`</step>`{=html}
`</steps_separated>`{=html} `</custom>`{=html}
`<caseid>`{=html}C87`</caseid>`{=html}
`<status>`{=html}Passed`</status>`{=html} `<assignedto>`{=html}Tester
Ro29`</assignedto>`{=html} `<inprogress>`{=html}`</inprogress>`{=html}
`<changes>`{=html} `<change>`{=html}
`<createdon>`{=html}2022-06-26T11:10:02Z`</createdon>`{=html}
`<createdby>`{=html}Tester Ro29`</createdby>`{=html}
`<status>`{=html}Passed`</status>`{=html}
`<assignedto>`{=html}`</assignedto>`{=html}
`<comment>`{=html}`</comment>`{=html}
`<version>`{=html}`</version>`{=html}
`<elapsed>`{=html}`</elapsed>`{=html}
`<defects>`{=html}`</defects>`{=html} `<custom>`{=html}
`<step_results>`{=html}`<step>`{=html} `<content>`{=html}Accesare URL
https://mobexpert.ro/`</content>`{=html}`<expected>`{=html}Se va
deschide pagina principala a
site-ului`</expected>`{=html}`<actual>`{=html}`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Selectare dintr-o categorie de produse
un anumit produs`</content>`{=html}`<expected>`{=html}Se va deschide o
noua pagina in care va fi afisat produsul
selectat`</expected>`{=html}`<actual>`{=html}`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Apasare buton \"Adauga la
favorite\"`</content>`{=html}`<expected>`{=html}Se va adauga produsul in
lista de favorite
`</expected>`{=html}`<actual>`{=html}`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Afisare pagina cu
produs`</content>`{=html}`<expected>`{=html}In dreapta sus in butonul cu
lista de produse favorite se va modifica cantitatea de produse
favorite.`</expected>`{=html}`<actual>`{=html}`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`</step_results>`{=html} `</custom>`{=html} `</change>`{=html}
`<change>`{=html}
`<createdon>`{=html}2022-06-23T21:08:59Z`</createdon>`{=html}
`<createdby>`{=html}Tester Ro29`</createdby>`{=html}
`<status>`{=html}Untested`</status>`{=html}
`<assignedto>`{=html}`</assignedto>`{=html}
`<comment>`{=html}`</comment>`{=html}
`<version>`{=html}`</version>`{=html}
`<elapsed>`{=html}`</elapsed>`{=html}
`<defects>`{=html}`</defects>`{=html} `</change>`{=html}
`</changes>`{=html} `</test>`{=html} `<test>`{=html}
`<id>`{=html}T307`</id>`{=html}
```{=html}
<title>
```
Verficarea functiei hover pe produsele afisate in lista de produse
favorite si modificarile asupra produselor
```{=html}
</title>
```
                                        <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-3</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C90</caseid>
                                                                <status>Passed</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-24T20:03:15Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Passed</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                                            <change>
                        <createdon>2022-06-23T21:08:59Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                                                                                                                                                                                                                                <test>
                <id>T308</id>
                <title>Selectarea unui produs</title>
                                                <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references></references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C181</caseid>
                                                                <status>Passed</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-24T19:38:23Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Passed</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                                            <change>
                        <createdon>2022-06-23T21:08:59Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                </tests>
                            </section>

`</sections>`{=html} `</run>`{=html}



```{=html}
<?xml version="1.0" encoding="UTF-8"?>
```
`<run>`{=html} `<id>`{=html}R34`</id>`{=html} `<name>`{=html}Ca
utilizator vreau sa pot adauga produse in cos`</name>`{=html}
`<description>`{=html}`</description>`{=html}
`<config>`{=html}`</config>`{=html}
`<createdon>`{=html}2022-06-24T19:47:09Z`</createdon>`{=html}
`<completed>`{=html}false`</completed>`{=html}
`<milestone>`{=html}`</milestone>`{=html} `<stats>`{=html}
`<passed>`{=html} `<percent>`{=html}67`</percent>`{=html}
`<count>`{=html}6`</count>`{=html} `</passed>`{=html} `<blocked>`{=html}
`<percent>`{=html}0`</percent>`{=html}
`<count>`{=html}0`</count>`{=html} `</blocked>`{=html}
`<untested>`{=html} `<percent>`{=html}22`</percent>`{=html}
`<count>`{=html}2`</count>`{=html} `</untested>`{=html}
`<retest>`{=html} `<percent>`{=html}0`</percent>`{=html}
`<count>`{=html}0`</count>`{=html} `</retest>`{=html} `<failed>`{=html}
`<percent>`{=html}11`</percent>`{=html}
`<count>`{=html}1`</count>`{=html} `</failed>`{=html} `</stats>`{=html}
`<sections>`{=html}
```{=html}
<section>
```
`<name>`{=html}Test Cases`</name>`{=html}
`<description>`{=html}`</description>`{=html} `<tests>`{=html}
`<test>`{=html} `<id>`{=html}T316`</id>`{=html}
```{=html}
<title>
```
Verificarea posibilitatii selectarii unui produs
```{=html}
</title>
```
                                        <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-7</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C196</caseid>
                                                                <status>Passed</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-24T19:56:52Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Passed</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                                            <change>
                        <createdon>2022-06-24T19:47:09Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                                                                                                                                                                                                                                <test>
                <id>T317</id>
                <title>Verificarea  deschiderii unei noi pagini cu detaliile produsului selectat</title>
                                                <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-7</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C198</caseid>
                                                                <status>Passed</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-24T19:57:03Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Passed</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                                            <change>
                        <createdon>2022-06-24T19:47:09Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                                                                                                                                                                                                                                <test>
                <id>T318</id>
                <title>Verificarea posibilitatii selectarii culorii dorite daca este cazul</title>
                                                <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-7</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C199</caseid>
                                                                <status>Passed</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-24T19:57:09Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Passed</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                                            <change>
                        <createdon>2022-06-24T19:47:09Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                                                                                                                                                                                                                                <test>
                <id>T319</id>
                <title>Verificarea functionalitatii butonului &quot;Adauga in cos&quot; si incrementarea acestuia</title>
                                                <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-7</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                        <steps_separated><step>

`<index>`{=html}1`</index>`{=html}`<content>`{=html}Accesare Url
https://mobexpert.ro/`</content>`{=html}`<expected>`{=html}Se deschide
pagina principala a site-ului`</expected>`{=html}`</step>`{=html}
`<step>`{=html} `<index>`{=html}2`</index>`{=html}`<content>`{=html}Se
face hover pe categoria \"Canapele\" din pagina
principala`</content>`{=html}`<expected>`{=html}Apare un pop up cu
diverse categorii de canapele `</expected>`{=html}`</step>`{=html}
`<step>`{=html} `<index>`{=html}3`</index>`{=html}`<content>`{=html}Se
apasa click pe categoria \"Canapele
stofa\"`</content>`{=html}`<expected>`{=html}Se deschide o pagina noua
cu toate produsele din categoria
selectata`</expected>`{=html}`</step>`{=html} `<step>`{=html}
`<index>`{=html}4`</index>`{=html}`<content>`{=html}Se apasa click pe
\"Cergy, canapea stofa , 2
locuri\"`</content>`{=html}`<expected>`{=html}Se deschide o noua pagina
doar cu produsul selectat`</expected>`{=html}`</step>`{=html}
`<step>`{=html} `<index>`{=html}5`</index>`{=html}`<content>`{=html}Se
face click pe butonul \"Adauga in
cos\"`</content>`{=html}`<expected>`{=html}Se deschide o noua pagina a
cosului de cumparaturi in care este incrementat produsul
selectat`</expected>`{=html}`</step>`{=html} `</steps_separated>`{=html}
`</custom>`{=html} `<caseid>`{=html}C200`</caseid>`{=html}
`<status>`{=html}Failed`</status>`{=html} `<assignedto>`{=html}Tester
Ro29`</assignedto>`{=html} `<inprogress>`{=html}`</inprogress>`{=html}
`<defects>`{=html}AB-11`</defects>`{=html} `<changes>`{=html}
`<change>`{=html}
`<createdon>`{=html}2022-06-24T20:01:34Z`</createdon>`{=html}
`<createdby>`{=html}Tester Ro29`</createdby>`{=html}
`<status>`{=html}Failed`</status>`{=html}
`<assignedto>`{=html}`</assignedto>`{=html}
`<comment>`{=html}`</comment>`{=html}
`<version>`{=html}`</version>`{=html}
`<elapsed>`{=html}`</elapsed>`{=html}
`<defects>`{=html}AB-11`</defects>`{=html} `<custom>`{=html}
`<step_results>`{=html}`<step>`{=html} `<content>`{=html}Accesare Url
https://mobexpert.ro/`</content>`{=html}`<expected>`{=html}Se deschide
pagina principala a site-ului`</expected>`{=html}`<actual>`{=html}S-a
deschis pagina principala a
site-ului`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Se face hover pe categoria
\"Canapele\" din pagina
principala`</content>`{=html}`<expected>`{=html}Apare un pop up cu
diverse categorii de canapele `</expected>`{=html}`<actual>`{=html}Este
afisat pop ul cu diverse categorii de
canapele`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Se apasa click pe categoria \"Canapele
stofa\"`</content>`{=html}`<expected>`{=html}Se deschide o pagina noua
cu toate produsele din categoria
selectata`</expected>`{=html}`<actual>`{=html}S-a deschis o noua pagina
cu toate produsele din categoria
selectata`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Se apasa click pe \"Cergy, canapea
stofa , 2 locuri\"`</content>`{=html}`<expected>`{=html}Se deschide o
noua pagina doar cu produsul
selectat`</expected>`{=html}`<actual>`{=html}S-a deschis o noua pagina
doar cu produsul
selectat`</actual>`{=html}`<status>`{=html}Passed`</status>`{=html}`</step>`{=html}
`<step>`{=html} `<content>`{=html}Se face click pe butonul \"Adauga in
cos\"`</content>`{=html}`<expected>`{=html}Se deschide o noua pagina a
cosului de cumparaturi in care este incrementat produsul
selectat`</expected>`{=html}`<actual>`{=html}Butonul \"Adauga in cos\"
nu exista, are in site denumirea de \"Comanda/Rezerva in
magazin\"`</actual>`{=html}`<status>`{=html}Failed`</status>`{=html}`</step>`{=html}
`</step_results>`{=html} `</custom>`{=html} `</change>`{=html}
`<change>`{=html}
`<createdon>`{=html}2022-06-24T19:47:09Z`</createdon>`{=html}
`<createdby>`{=html}Tester Ro29`</createdby>`{=html}
`<status>`{=html}Untested`</status>`{=html}
`<assignedto>`{=html}`</assignedto>`{=html}
`<comment>`{=html}`</comment>`{=html}
`<version>`{=html}`</version>`{=html}
`<elapsed>`{=html}`</elapsed>`{=html}
`<defects>`{=html}`</defects>`{=html} `</change>`{=html}
`</changes>`{=html} `</test>`{=html} `<test>`{=html}
`<id>`{=html}T320`</id>`{=html}
```{=html}
<title>
```
Se va verifica daca se afiseaza o noua pagina cu produsul adaugat in cos
```{=html}
</title>
```
                                        <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-7</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C201</caseid>
                                                                <status>Passed</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-24T20:01:43Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Passed</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                                            <change>
                        <createdon>2022-06-24T19:47:09Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                                                                                                                                                                                                                                <test>
                <id>T321</id>
                <title>Verificarea functionlitatii simbolului &quot;Cart&quot; si deschiderea unei noi pagini care sa afiseze produsul adaugat in cos</title>
                                                <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-7</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C202</caseid>
                                                                <status>Passed</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-24T20:02:04Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Passed</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                                            <change>
                        <createdon>2022-06-24T19:47:09Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                                                                                                                                                                                                                                <test>
                <id>T322</id>
                <title>Verificarea functionalitatii simbolului &quot;&apos;X&apos;&quot;  din partea stanga pentru produsul afisat</title>
                                                <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-7</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C205</caseid>
                                                                <status>Passed</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-24T20:02:19Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Passed</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                                            <change>
                        <createdon>2022-06-24T19:47:09Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                                                                                                                                                                    <test>
                <id>T323</id>
                <title>Verficarea functionalitatii simbolului  &quot;+&apos;&quot; daca creste cantitatea de produse din cos</title>
                                                <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-7</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C206</caseid>
                                                                <status>Untested</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-24T19:47:09Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                                                                                                                                                                    <test>
                <id>T324</id>
                <title>Verficarea functionalitatii simbolului  &quot;-&quot; daca scade cantitatea de produse din cos</title>
                                                <template>Test Case (Steps)</template>
                                                                <type>Other</type>
                                                                <priority>Medium</priority>
                                                                <estimate></estimate>
                                                                                                <references>AB-7</references>
                                                                                <custom>
                                        <automation_type><id>0</id><value> None</value></automation_type>
                                    </custom>
                                                <caseid>C207</caseid>
                                                                <status>Untested</status>
                                                                <assignedto>Tester Ro29</assignedto>
                                                                <inprogress></inprogress>
                                                                                                                                                                                                                                                                                                                                                        <changes>
                                                            <change>
                        <createdon>2022-06-24T19:47:09Z</createdon>
                                                                        <createdby>Tester Ro29</createdby>
                                                <status>Untested</status>
                                                <assignedto></assignedto>
                                                <comment></comment>
                                                                        <version></version>
                                                                                                                        <elapsed></elapsed>
                                                                                                                        <defects></defects>
                                                                                                                    </change>
                                    </changes>
            </test>
                                </tests>
                            </section>

`</sections>`{=html} `</run>`{=html}
